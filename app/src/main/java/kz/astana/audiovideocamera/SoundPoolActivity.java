package kz.astana.audiovideocamera;

import android.media.SoundPool;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class SoundPoolActivity extends AppCompatActivity implements SoundPool.OnLoadCompleteListener {

    private final int MAX_STREAMS = 5;

    private SoundPool soundPool;
    private int soundIdShot, soundIdExplosion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sound_pool);

        SoundPool.Builder builder = new SoundPool.Builder();
        builder.setMaxStreams(MAX_STREAMS);
        soundPool = builder.build();
        soundPool.setOnLoadCompleteListener(this);

        soundIdShot = soundPool.load(SoundPoolActivity.this, R.raw.shot, 1);

        try {
            soundIdExplosion = soundPool.load(getAssets().openFd("explosion.ogg"), 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void play(View view) {
        soundPool.play(soundIdShot, 1, 1, 0, 10, 2);
        soundPool.play(soundIdExplosion, 1, 1, 0, 0, 1);
    }

    @Override
    public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
        Toast.makeText(SoundPoolActivity.this, "SampleId: " + sampleId + ", status: " + status, Toast.LENGTH_LONG).show();
    }
}