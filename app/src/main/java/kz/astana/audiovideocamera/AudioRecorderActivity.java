package kz.astana.audiovideocamera;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class AudioRecorderActivity extends AppCompatActivity {

    private int myBufferSize = 8192;
    private AudioRecord audioRecord;
    private boolean isReading = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audio_recorder);

        int sampleRate = 8000;
        int channelConfig = AudioFormat.CHANNEL_IN_MONO;
        int audioFormat = AudioFormat.ENCODING_PCM_16BIT;
        int minInternalBufferSize = AudioRecord.getMinBufferSize(sampleRate, channelConfig, audioFormat);
        int internalBufferSize = minInternalBufferSize * 4;

        audioRecord = new AudioRecord(MediaRecorder.AudioSource.MIC, sampleRate, channelConfig, audioFormat, internalBufferSize);
    }

    public void startRecord(View view) {
        audioRecord.startRecording();
        int recordingState = audioRecord.getRecordingState();
        Toast.makeText(AudioRecorderActivity.this, "State: " + recordingState, Toast.LENGTH_LONG).show();
    }

    public void stopRecord(View view) {
        audioRecord.stop();
    }

    public void startRead(View view) {
        isReading = true;
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (audioRecord == null) {
                    return;
                }
                byte[] myBuffer = new byte[myBufferSize];
                int readCount = 0;
                int totalCount = 0;
                while (isReading) {
                    readCount = audioRecord.read(myBuffer, 0, myBufferSize);
                    totalCount += readCount;
                    Log.d("Hello", readCount + ":" + totalCount);
                }
                Toast.makeText(AudioRecorderActivity.this, "Count: " + totalCount, Toast.LENGTH_LONG).show();
            }
        }).start();
    }

    public void stopRead(View view) {
        isReading = false;
    }
}