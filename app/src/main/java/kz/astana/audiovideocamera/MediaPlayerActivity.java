package kz.astana.audiovideocamera;

import android.content.ContentUris;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MediaPlayerActivity extends AppCompatActivity implements MediaPlayer.OnPreparedListener, MediaPlayer.OnCompletionListener {

    private String DATA_HTTP = "http://mp3clan.top/dl.php?type=get&s=cb4ff5630ce2457f5862cc9a352e59a3&u=VlBnajVXelVQYk1ZcGY2UGVnclFUd1hINzdCS1VJVnI3NmRxZFpvYXBoRjVNbVJzWTh4V2tvNFNzY3p3UVl1Y3ZoWkJkc0NHV1ZZZVphanZDRUZJaGZST3kxb1VIMWJ6UlBUc3NNaEpIZ05Vdm5wQTJYWmZwR21OYVcwM0g2U2I4bmZobVdTREVLTld2SjNYUG9INHgxekN1b0l0RFRCRk9yTTUzOHdVWlp4ZTc4eHQwQmpUaHlSQ1JQbzNWZlJIeHFEMWFENWRzV1JrSjVabVBadGpUbGx1RlJWNDVkMmVHSlZhcThjak5oV3JibmhxWHJoQjlPS28wYjNDamgwdy9uSGtDQzlCT0Zsc2hETEw4RlVQQ1c1NTZyMlZ1U0JteGFka1FrbnZhY0o3Y2UxeS9TeStieXdqQ2NnZk5kSXojIyM2Njk4NjkzNDcwNDAwIyMjMjUy&tid=-2001784291_77784291&source=aHR0cDovLzk1LjIxMS4xNjguODQvZ2V0LnBocA==&tt=1&name=Mammoth_Wvh_-_Distance";
    private String DATA_STREAM = "http://online.radiorecord.ru:8101/rr_128";
    private String DATA_SD = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC) + "/music.mp3";
    private Uri DATA_URI = ContentUris.withAppendedId(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, 13359);

    private MediaPlayer mediaPlayer;
    private AudioManager audioManager;
    private CheckBox loop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_media_player);

        audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
        loop = findViewById(R.id.loop);
        loop.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (mediaPlayer != null) {
                    mediaPlayer.setLooping(isChecked);
                }
            }
        });
    }


    public void onClickData(View view) {
        try {
            switch (view.getId()) {
                case R.id.startHttp:
                    mediaPlayer = new MediaPlayer();
                    mediaPlayer.setDataSource(DATA_HTTP);
                    mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                    mediaPlayer.setOnPreparedListener(this);
                    mediaPlayer.prepareAsync();
                    break;
                case R.id.startStream:
                    mediaPlayer = new MediaPlayer();
                    mediaPlayer.setDataSource(DATA_STREAM);
                    mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                    mediaPlayer.setOnPreparedListener(this);
                    mediaPlayer.prepareAsync();
                    break;
                case R.id.startSD:
                    Log.d("Hello", "SD");
                    mediaPlayer = new MediaPlayer();
                    mediaPlayer.setDataSource(DATA_SD);
                    mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                    mediaPlayer.setOnPreparedListener(this);
                    mediaPlayer.prepare();
                    mediaPlayer.start();
                    break;
                case R.id.startUri:
                    mediaPlayer = new MediaPlayer();
                    mediaPlayer.setDataSource(MediaPlayerActivity.this, DATA_URI);
                    mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                    mediaPlayer.setOnPreparedListener(this);
                    mediaPlayer.prepare();
                    mediaPlayer.start();
                    break;
                case R.id.startRaw:
                    mediaPlayer = MediaPlayer.create(MediaPlayerActivity.this, R.raw.explosion);
                    mediaPlayer.start();
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (mediaPlayer == null) {
            return;
        }
        mediaPlayer.setLooping(loop.isChecked());
        mediaPlayer.setOnCompletionListener(this);
    }

    public void onClick(View view) {
        if (mediaPlayer == null) {
            return;
        }
        switch (view.getId()) {
            case R.id.play:
                if (!mediaPlayer.isPlaying()) {
                    mediaPlayer.start();
                }
                break;
            case R.id.pause:
                if (mediaPlayer.isPlaying()) {
                    mediaPlayer.pause();
                }
                break;
            case R.id.stop:
                mediaPlayer.stop();
                break;
            case R.id.backward:
                mediaPlayer.seekTo(mediaPlayer.getCurrentPosition() - 5000);
                break;
            case R.id.forward:
                mediaPlayer.seekTo(mediaPlayer.getCurrentPosition() + 5000);
                break;
            case R.id.info:
                String text = "Playing: " + mediaPlayer.isPlaying() +
                        "\nTime: " + mediaPlayer.getCurrentPosition() + "/" + mediaPlayer.getDuration() +
                        "\nLooping: " + mediaPlayer.isLooping() +
                        "\nVolume: " + audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
                Toast.makeText(MediaPlayerActivity.this, text, Toast.LENGTH_LONG).show();
                break;
        }
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        mp.start();
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        Toast.makeText(MediaPlayerActivity.this, "Finished", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {
    }
}